//  FILE NAME:          app.js
//  DESCRIPTION:        Javascripts for Site
//  SITE URL:           http://site.com
//  STRUCTURE:
//                1.0 BLANK
//                2.0 BLANK

// 1.0 BLANK

$(function(){
	// alert('DOM loaded!');
	
	$('footer nav ul li a').unbind('click').click( app.changeContent );
	$('a.video').unbind('click').click( app.playVideo );
	
	window.addEventListener('load', function() {
	    new FastClick(document.body);
	}, false);

	document.addEventListener('deviceready', app.initApp, true);

});

// 2.0 BLANK
$(window).load(function(){
	var width = (window.devicePixelRatio > 1) ? screen.width/window.devicePixelRatio : screen.width;
		
	if (width >= 800) {
		$('#viewport').attr('content', 'width=800');
	}
	
	// alert('Content loaded!');
});

var app = {
	
	changeContent: function() {
		var clicked = $(this);
		var el = clicked.attr('id').replace('nav_', 'content_');
		
		$('#main > div').removeClass('show');
		$('#'+el).addClass('show');
		
		if (el == "content_animation")
			app.playAnimation();
	
		// change active button
		$('footer nav ul li').removeClass('active');
		clicked.parent('li').addClass('active');
	},
	
	initApp: function() {
		console.log("Initializing App");
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, app.gotFS, app.fail);

		app.sendVideoCounts();
		// only fires when online changes...
		document.addEventListener("online", app.sendVideoCounts, false);
	
	},
	
	sendVideoCounts: function() {
		console.log('sendVideoCounts all three');
		app.localstorage.sendCount(1);
		app.localstorage.sendCount(2);
		app.localstorage.sendCount(3);
			
	},

	playAnimation: function() {
		var video = $('#content_animation video');
		video[0].load();
		
		app.localstorage.updateCount(3);
		
		video.on('canplay', function() {
			video[0].currentTime=0;
			video[0].play();
			app.fullScreen(video);
		});	
	},
	
	// video schtuff
	playVideo: function() {
		var clicked = $(this);
		var el = clicked.attr('id');
		console.log('in playVideo and clicked '+el);
		// update play count.
		if (el == "ablation") {
			app.localstorage.updateCount(1);
			console.log('add to count 1');
		} else if (el == "convergent") {
			app.localstorage.updateCount(2);
			console.log('add to count 2');
		} else if (el == "fibrosis") {
			app.localstorage.updateCount(3);
			console.log('add to count 3');
		} else if (el == "nav_animation") {
			app.localstorage.updateCount(3);
			console.log('add to count 3');
		}


		var src = clicked.attr('data-src');
		
		var video = $('.video_player video');
		
		console.log(src);
		
		window.plugins.videoPlayer.play("file:///android_asset/www/"+src);
		
		/*
		if (video.attr('src') !== src) {
			
			video.attr('src', src);
			video[0].load();
						
			// wait til load is finished...
			video.on('canplay', function() {
				video[0].currentTime = 0;
				video[0].play();
				app.fullScreen(video);
			});
		} else {
			// already loaded
			video[0].currentTime = 0;
			video[0].play();
			app.fullScreen(video);
		}
		*/
		
	},
	
	// check if video is playing and if so go Fullscreen
	// on close video or minimize, pause the video
	// on video end, exit fullscreen and pause the video
	fullScreen: function(iVideo) {
		
		// if video is started go to Fullscreen
		if (iVideo[0].currentTime > 0)
			iVideo[0].webkitEnterFullScreen();
		else {
			setTimeout(function() {
				app.fullScreen(iVideo);
			}, 100);
		}

		// if exit fullscreen with done button or minimize button			
		iVideo.on('webkitendfullscreen', function() {
			iVideo[0].pause();
		});
		
		// close when video finishes	
		iVideo.on('ended', function() {
			iVideo[0].webkitExitFullScreen();
			iVideo[0].pause();
		});
	},
	
	// custom logo
	checkForLogo: function() {
		
		//app.imageUrl = app.fileSystemPath+'/cplogo.png';
		app.imageUrl = app.fileSystemPath+'/logo.png';
		console.log('imageUrl:'+app.imageUrl);
		$.ajax({
			type: "HEAD",
			url: app.imageUrl,
			error: function(request, status) {
				console.log(app.imageUrl+': NOT FOUND');
				app.openLoginForm();
			},
			success: function() {
				console.log('image exists');
				app.makeLogoImage(app.imageUrl);
			}
		});
	},
	
	makeLogoImage: function() {
		// navigator.notification.alert('Make Logo Image', function() {}, "Error", "Done");
		$('#modal_bg').on('webkitTransitionEnd', function() {
			$('#modal_bg').hide();
		}).addClass('hide');
		
		$('#app_brand').attr('src', app.imageUrl).removeClass('hide');
	},
	
	gotFS: function(fileSystem) {
		console.log('got fileSystem');
		app.fileSystemPath = cordova.file.dataDirectory;
		//app.fileSystemPath = fileSystem.root.fullPath;
		app.checkForLogo();
	},
	
	openLoginForm: function() {
		console.log('openLoginForm');
		if (navigator.connection.type == "none") {
			navigator.notification.alert('You must be connected to the internet in order to get your custom logo', function() {}, "Error", "Done");
		} else {
			// open form
			$('#modal_bg').show(1, function() {
				$('#modal_bg').removeClass('hide');
			});
			// bind submit to requestLogoJson
			$('#login_form #username').on('focus', function() {
				if ($(this).val() == "email")
					$(this).val('');
			});
			
			$('#login_form #password').on('focus', function() {
				if ($(this).val() == "password")
					$(this).val('');
			});
			
			$('#login_form button').click(function(e){ 
				e.preventDefault();
				app.requestLogoJson(); 
			});
		}
	},
	
	requestLogoJson: function() {
		
		var user = encodeURIComponent($('#login_form #username').val());
		var password = $('#login_form #password').val();
		
		console.log('user:'+user+' - pass:'+password);
		
		$.jsonp( {
			url: "http://sites.interactm.com/ncontact/public/data/"+user+'/'+password+'/myFunction',
			callback: "myFunction",
			cache: false,
			crossDomain: true,
			dataType: "jsonp",
			
			error: function(request, status) {
				console.log('request', request);
				console.log('status', status);
				navigator.notification.alert('There is a problem with the Login validation server.  Please try again later.', null, "Error", "Done");
			},

			success: function(response) {
				console.log('response', response);
				if (response[0].status == "Success") {
					app.downloadLogo(response[0].result);
                
                    // save ID into local storage
                    app.localstorage.saveId(response[0].id);
                
				} else {
					navigator.notification.alert(response[0].result, function() {}, response[0].status, "Ok");
				}
			}
		});
	},
	
	// download image if json request comes back successful
	downloadLogo: function(iFilePath) {

		function fail(evt) {
        	console.log(evt.target.error.code);
        }

		window.requestFileSystem(LocalFileSystem.PERSISTENT, 
			0, 
			function onFileSystemSuccess(fileSystem) {
				fileSystem.root.getFile(
					"dummy.html",
					{create: true, exclusive: false},

					function gotFileEntry(fileEntry) {
						var filePath = iFilePath;
						var sPath = fileEntry.fullPath.replace("dummy.html",""); // remove filename from new file
						
						var ft = new FileTransfer();
						var statusDom = document.getElementById('downloadStatus');
						
						// sample filename parser from url
						var filename = filePath.substr(filePath.lastIndexOf("/")+1,filePath.length);
						var ext = filename.substr(filename.lastIndexOf('.') + 1);

						fileEntry.remove();

						ft.onprogress = function(progressEvent) {
							console.log('on progress:'+progressEvent.loaded+'/'+progressEvent.total);
							if (progressEvent.lengthComputable) {
								var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
								statusDom.innerHTML = perc + "% loaded...";
								console.log(perc + "% loaded...");
							} else {
								if(statusDom.innerHTML == "") {
									statusDom.innerHTML = "Loading";
								} else {
									statusDom.innerHTML += ".";
								}
							}
						};
											
						ft.download(
							filePath, // file you are downloading
							//sPath + "cplogo." +ext, // path to save to...
							app.fileSystemPath+"logo."+ext,
							function(theFile) { // download complete
								console.log("download complete: " + theFile.toURL());
								// navigator.notification.alert('Custom logo downloaded successfully', function() {}, "Success", "Done");
								app.makeLogoImage(app.imageUrl);
								// showLink(theFile.toURI());
							},
							function(error) { // error
								console.log("download error source " + error.source);
								console.log("download error target " + error.target);
								console.log("upload error code: " + error.code);
								console.log(error);
							},
							true // trust all hosts (defaults to false)
						);
					},
					fail
				);
			},
			fail
		);
	}
};





