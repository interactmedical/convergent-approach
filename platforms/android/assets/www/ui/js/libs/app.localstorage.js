app.localstorage = {
	
	saveId: function(iId) {
		
		localStorage.userid =  iId;
		localStorage["count_1"] = 0;
		localStorage["count_2"] = 0;
		localStorage["count_3"] = 0;
	},
	
	getId: function() {
		console.log("user ID:"+localStorage.userid);
		return localStorage.userid;
	},
	
	getCount: function() {
		return localStorage.count;
	},
	
	updateCount: function(iVideoNum) {
		var curCount = parseFloat(localStorage["count_"+iVideoNum]);
		console.log("in updateCount and sending curCount:"+curCount);
		localStorage["count_"+iVideoNum] = curCount + 1;
		app.localstorage.sendCount(iVideoNum);
		return;	
	},
	
	sendCount: function(iVideoNum) {
		console.log("sendCount:"+localStorage["count_"+iVideoNum]);
		console.log("localStorage.userid:"+localStorage.userid);
		// if nothing to send, then don't make the connection...
		if (!localStorage["count_"+iVideoNum] > 0)
			return;

		if (navigator.connection.type == "NONE") {
			// try again later and just leave it all alone :)
		} else {

			$.jsonp( {
				url: "http://sites.interactm.com/ncontact/public/data/count/"+localStorage.userid+'/'+iVideoNum+'/'+localStorage["count_"+iVideoNum]+'/myFunction',
				callback: "myFunction",
				cache: false,
				crossDomain: true,
				dataType: "jsonp",
				
				error: function(request, status) {
					console.log('request', request);
					console.log('status', status);
				},
	
				success: function(response) {
					
					console.log('the countSent', response[0].status);
					if (response[0].version > 14) {
						navigator.notification.alert('There is a newer version of this app available at http://apps.interactm.com/ncontact/android', null, "Update Available", "Done");
					}

					if (response[0].status == "Success") {						
						localStorage["count_"+iVideoNum] = 0;	                
					} else {
						// try again later.
					}
				}
			});
		
		} // end else
	}
	
	
};